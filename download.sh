#!/bin/bash

#####################################################
# CONFIG
#####################################################
# ENDDIR - This is now depreciated, please use...
# echo "/mnt/unionfs" > lists/enddir.txt
ENDDIR=$(cat lists/enddir.txt)

#####################################################
# END CONFIG
#####################################################
IFS=$'\n'
for i in "$@"; do
    case ${i} in
    -t | --twitch)
        TWITCH="true"
        ;;
    -y | --youtube)
        YOUTUBE="true"
        ;;
    -d | --deezer)
        DEEZER="true"
        ;;
    *)
        TWITCH="true"
        YOUTUBE="true"
        ;;
    esac
done
while true; do
    if [ "${YOUTUBE}" == "true" ]; then
        if [ -f lists/youtube.txt ]; then
            for i in $(cat lists/youtube.txt); do
                CHANNELID=$(echo ${i} | sed 's/ #.*//g')
                CHANNELNAME=$(echo ${i} | sed 's/.* #//g' | sed 's/ -.*//g')
                if [ "$(echo ${i} | grep '\-channel')" ]; then
                    CHANNELTYPE="channel"
                elif [ "$(echo ${i} | grep '\-user')" ]; then
                    CHANNELTYPE="user"
                fi
                if [ "$(echo ${i} | grep '\-playlist')" ]; then
                    PLAYLIST="true"
                else
                    PLAYLIST="false"
                fi
                echo "######################################"
                echo "# Now downloading - Youtube: ${CHANNELNAME}"
                echo "######################################"
                echo "# Channel Type: ${CHANNELTYPE}"
                echo "# Playlist: ${PLAYLIST}"
                echo "######################################"
                if [[ "${CHANNELTYPE}" == "channel" && "${PLAYLIST}" == "false" ]]; then
                    youtube-dl "https://www.youtube.com/channel/${CHANNELID}/videos" -f bestvideo+bestaudio/best \
                        --restrict-filename \
                        --no-overwrites \
                        --geo-bypass \
                        --ignore-errors \
                        --embed-subs \
                        --all-subs \
                        --convert-subs srt \
                        --download-archive lists/archive.txt \
                        --write-description \
                        --merge-output-format mkv \
                        --match-filter '!is_live' \
                        -o '%(uploader)s/%(upload_date)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                fi
                if [[ "${CHANNELTYPE}" == "user" && "${PLAYLIST}" == "false" ]]; then
                    youtube-dl "https://www.youtube.com/user/${CHANNELID}/videos" -f bestvideo+bestaudio/best \
                        --restrict-filename \
                        --no-overwrites \
                        --geo-bypass \
                        --ignore-errors \
                        --embed-subs \
                        --all-subs \
                        --convert-subs srt \
                        --download-archive lists/archive.txt \
                        --write-description \
                        --merge-output-format mkv \
                        --match-filter '!is_live' \
                        -o '%(playlist_uploader)s/%(upload_date)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                fi
                if [[ "${CHANNELTYPE}" == "user" && "${PLAYLIST}" == "true" ]]; then
                    youtube-dl "https://www.youtube.com/user/${CHANNELID}/playlists" -f bestvideo+bestaudio/best \
                        --restrict-filename \
                        --no-overwrites \
                        --geo-bypass \
                        --ignore-errors \
                        --embed-subs \
                        --all-subs \
                        --convert-subs srt \
                        --download-archive lists/archive.txt \
                        --write-description \
                        --merge-output-format mkv \
                        --match-filter '!is_live' \
                        -o '%(playlist_uploader)s/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                fi
                if [[ "${CHANNELTYPE}" == "channel" && "${PLAYLIST}" == "true" ]]; then
                    youtube-dl "https://www.youtube.com/channel/${CHANNELID}/playlists" -f bestvideo+bestaudio/best \
                        --restrict-filename \
                        --no-overwrites \
                        --geo-bypass \
                        --ignore-errors \
                        --embed-subs \
                        --all-subs \
                        --convert-subs srt \
                        --download-archive lists/archive.txt \
                        --write-description \
                        --merge-output-format mkv \
                        --match-filter '!is_live' \
                        -o '%(playlist_uploader)s/%(playlist)s/%(playlist_index)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Youtube/$(dirname {}) && \
					cp -v $(dirname {})/* '${ENDDIR}'/Youtube/$(dirname {}) && \
					rm -f $(dirname {})/*'
                fi
                find . -type d -empty -delete
            done
        fi
    fi
    if [ "${TWITCH}" == "true" ]; then
        if [ -f lists/twitch.txt ]; then
            for i in $(cat lists/twitch.txt); do
                if [ "$(echo ${i} | grep '\-vod')" ]; then
                    DOWNLOAD_VOD="true"
                elif [ "$(echo ${i} | grep '\-clips')" ]; then
                    DOWNLOAD_CLIPS="true"
                fi
                CHANNELNAME=$(echo ${i} | sed 's/ -.*//g')
                if [ "${DOWNLOAD_VOD}" == "true" ]; then
                    echo "######################################"
                    echo "# Now downloading - Twitch: ${CHANNELNAME} VODS"
                    echo "######################################"

                    youtube-dl --external-downloader aria2c \
                        "https://www.twitch.tv/${CHANNELNAME}/videos/all" \
                        --no-overwrites \
                        --download-archive lists/archive.txt \
                        --ignore-errors \
                        --match-filter '!is_live & extractor = "twitch:vod"' \
                        -o '%(uploader)s/%(extractor_key)s/%(upload_date)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Twitch/$(dirname {}) && \
                    cp {} '${ENDDIR}'/Twitch/{} && \
                    rm -f {}'
                fi
                if [ "${DOWNLOAD_CLIPS}" == "true" ]; then
                    echo "######################################"
                    echo "# Now downloading - Twitch: ${CHANNELNAME} CLIPS"
                    echo "######################################"

                    echo " Grabbing Clip data...."
                    ./bin/twitch-clips-grabber --clientID="ult4gydhjy5exy5euhcgzjx9q5t2bs" \
                        --channelName="${CHANNELNAME}" >batch.txt
                    echo "..Done"

                    youtube-dl -a batch.txt \
                        --restrict-filename \
                        --no-overwrites \
                        --ignore-errors \
                        --download-archive lists/archive.txt \
                        --match-filter 'extractor = "twitch:clips"' \
                        -o '%(creator)s/%(extractor_key)s/%(upload_date)s - %(title)s.%(ext)s' \
                        --exec 'mkdir -p '${ENDDIR}'/Twitch/$(dirname {}) && \
                    cp {} '${ENDDIR}'/Twitch/{} && \
                    rm -f {}'

                    rm -f batch.txt
                fi
                find . -type d -empty -delete
                find . -type d -empty -delete
            done
        fi
    fi
    if [ "${DEEZER}" == "true" ]; then
        if [ -f lists/twitch.txt ]; then
            for i in $(cat lists/deezer.txt); do
                ARTISTID=$(echo ${i} | sed 's/ #.*//g')
                ARTISTNAME=$(echo ${i} | sed 's/.* #//g')
                echo "######################################"
                echo "# Now downloading - Deezer: ${ARTISTNAME}"
                echo "######################################"
                ./bin/SMLoadr-linux-x64 -q "FLAC" -p "${ENDDIR}/music/" "https://www.deezer.com/artist/${ARTISTID}"
            done
        fi
        echo "Cleaning up.."
        rm -fv downloadedSuccessfully.txt downloadedUnsuccessfully.txt downloadLinks.txt SMLoadr.log
        echo "Done."
    fi
    echo "######################################"
    echo "# Done, sleeping for 3 hours"
    echo "######################################"
    sleep 3h
done
