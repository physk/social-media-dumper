# Social Media Dumper

There are a few dependecies for this script to run
* ``youtube-dl``
* ``aria2c``
* ``twitch-clips-grabber``
* ``SMLoadr``

you can install them using the following commands
## youtube-dl
from [youtube-dl](https://github.com/ytdl-org/youtube-dl)
```
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl
```
## aria2c
```
sudo apt install aria2 -y
```
## twitch-clips-grabber
```
git clone https://gitlab.com/physk/social-media-dumper.git && \
cd social-media-dumper && \
mkdir lists && \
mkdir bin
```
Grab the latest release from [twitch-clips-grabber](https://gitlab.com/physk/twitch-clips-grabber/releases) named ``twitch-clips-grabber-linux.tar.gz``
```
wget https://gitlab.com/physk/twitch-clips-grabber/uploads/0262748885868fd546fb249ead991648/twitch-clips-grabber-linux.tar.gz && \
tar xfv twitch-clips-grabber-linux.tar.gz && \
mv twitch-clips-grabber bin/ && \
rm -f twitch-clips-grabber-linux.tar.gz
```
## SMLoadr
grab the latest release from [SMLoadr](https://git.fuwafuwa.moe/SMLoadrDev/SMLoadr/releases) and download it to /bin
```
unzip Smloadr-linux-x64_*.zip
```
---
### Set permissions on everything
```
chmod +x download.sh && \
chmod +x bin/twitch-clips-grabber
```
## Twitch
### Create you list and add your items
```
nano lists/twitch.txt
```

e.g.
```
shroud -vod -clips
syndicate -vod
```
Save and exit

## Youtube
### Create your list and add your items
```
nano lists/youtube.txt
```
e.g.
```
UCN7gplqpfnrT-mxjoTR4ENg #Tove Lo -channel
UCLecVrux63S6aYiErxdiy4w #BRUH Automation -channel
LoLChampSeries #LoL Esports -user -playlist
```
save and exit

## Change your download destination
change ENDDIR to where you files will end up
they will be like ``${ENDDIR}/Youtube/Channel_Name``, ``${ENDDIR}/Twitch/Channel_Name/TwitchVods/``, ``${ENDDIR}/Twtich/Channel_Name/TwitchClips``, ``${ENDDIR}/music``
```
nano download.sh
```
change ENDDIR to your liking
## Run your downloads
### Arguments
```
-y, --youtube  | Download Youtube
-t, --twitch   | Download Twitch
-d, --deezer   | Download Deezer
```
```
./download.sh
```
I recommend that you run it in a screen session as it will run forever
```
screen -mS social-dumper
```
```
./download.sh
```